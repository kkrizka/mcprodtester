#!/usr/bin/env python

# %% Important packages
import argparse

import pyAMI.client
import pyAMI_atlas.api as AtlasAPI

from mcprodtester import tags
from mcprodtester import request

# %% Input arguments
parser = argparse.ArgumentParser(description='Update tags definitions in AMI, creating new ones if necessary.')
parser.add_argument('request' , help='Request file')
parser.add_argument('--pretend','-p' , action='store_true', help='Print AMI command and do not execute.')

args = parser.parse_args()

# %% Setup clients to access ATLAS databases
ami = pyAMI.client.Client('atlas')
AtlasAPI.init()

# %% Open the request
request = request.load(args.request)
builder = tags.TagBuilder(request.get('tags',{}), ami)

# %% Loop over tags and try to register them
for tag in builder.tags:
    print(f'Processing {tag}')

    createTag=not tags.tag_valid(tag)

    tagobj=builder.tag(tag)

    # Special formatting
    tagType=tag[0]
    releaseName=tagobj.release.replace(',','_')

    # Prepare the AMI command (create or update)
    command=[]
    if createTag:
        command.append('AddAMITag')
        command.append(f'-transformName="{tagobj.transform}"')
    else:
        command.append('UpdateAMITag')
        command.append(f'-amiTag="{tag}"')
    command.append(f'-tagType="{tagType}"')
    command.append(f'-releaseName="{releaseName}"')
    command.append(f'-productionStep="{tagobj.productionStep}"')
    command.append(f'-description="{tagobj.description}"')

    # Add arguments
    escaper=str.maketrans({'\\':r'\\','"':r'\"'})
    for k,v in tagobj.args.items():
        if v is None:
            v=''

        # escape
        if type(v) is list:
            v=' '.join([r'"{}"'.format(v0.translate(escaper)) for v0 in v])
        v=v.translate(escaper)

        command.append(f'-{k}="{v}"')

    # Execute command
    if args.pretend:
        print(' '.join(command))
    else:
        result=ami.execute(command)

        lines=result.split('\n')
        for line in lines:
            if line.startswith('error'):
                print(f'\t{line[10:].strip()}')
            elif line.startswith('info'):
                print(f'\t{line[10:].strip()}')
