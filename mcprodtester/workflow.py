import os
import subprocess
import shutil

from pathlib import Path

class TransformNode:
    def __init__(self,name,tag,outtype,tags,args={},maxEvents=5,cores=1):
        """
        Configure the transform execution.

        Parameters
        ----------
        name : str
          Name of the job (used for workdir location, output file name)
        tag : str
          Tag with the transform definitoin.
        outtype : str
          Output file type.
        tags : tags.TagBuilder
          Tag database containing the requested tag.
        args : dict of str
          Extra arguments to add to transform.
        maxEvents : int
          Maximum number of events to run over.
        cores : int
          Number of cores that the transform can use.
        """
        # Tools
        self.tags=tags

        # Execution
        self.maxEvents=maxEvents
        self.cores=cores

        # Transform
        self.name=name
        self.tag=tag
        self.args=args
        self.outtype=outtype

    def run(self, input):
        # Prepare working directory
        workdir=os.environ.get('WORK_DIR',f'.cache/work')+f'/{self.name}'
        workdir=os.path.realpath(workdir)

        # cleanup if exists
        if os.path.exists(workdir):
            shutil.rmtree(workdir)
        os.makedirs(workdir)

        # Get tag info using AMI
        tag=self.tags.tag(self.tag)

        # Build the transform command
        output=f'{self.outtype}.{self.name}.root'

        trcmd=[tag.transform]
        trcmd+=tag.buildargs(ATHENA_CORE_NUMBER=self.cores)
        trcmd+=[f'--output{self.outtype}File',output]
        if tag.transform=='Gen_tf.py':
            # Input is job optrun(ions used to generate events
            trcmd+=[f'--jobConfig',str(input)]
        else:
            # Input is a file
            inpath=Path(input)
            for k,v in self.args: # Add node arguments
                trcmd+=[f'--{k}',v]
            if inpath.suffix=='.data': # This is a RAW bytestream
                trcmd+=[f'--inputBSFile',input]
            else: # This is a ROOT file
                intype=inpath.name.split('.')[0]
                trcmd+=[f'--input{intype}File',input]
        trcmd+=['--maxEvents',str(self.maxEvents)] # Just run over a few events for testing

        # Build the full command
        cmd=['run.sh']
        cmd+=[f'-j{self.cores}']
        cmd+=[tag.release]
        cmd+=trcmd

        # actually run it
        proc=subprocess.run(cmd, cwd=workdir)
        if proc.returncode!=0: # cleanup
            print('Error executing command!')
            return None

        # save the output
        datadir=os.environ.get('DATA_DIR','.cache/')
        datadir=os.path.realpath(os.path.join(datadir,'local'))
        os.makedirs(datadir, exist_ok=True)
        finaloutput=os.path.join(datadir,output)
        workoutput =os.path.join(workdir,output)
        shutil.copyfile(workoutput, finaloutput)

        # return final output location for next step
        return finaloutput

class RucioNode:
    def __init__(self,scope,dsid,client):
        self.client=client

        self.scope=scope
        self.dsid=dsid

    def run(self, input=None):
        dsid=f'{self.scope}:{self.dsid}'

        # Get local file location
        dldir=os.environ.get('DATA_DIR','.cache/')
        dldir=os.path.realpath(dldir)
        if not os.path.exists(dldir):
            os.makedirs(dldir)
        localpath=os.path.join(dldir,self.scope,self.dsid)
        if os.path.exists(localpath):
            print(f'Found locally: {localpath}')
        else:
            print(f'Downloading {dsid}')

            # Need to use lsetup to get access to all of the necessary
            # protocols.
            cmd=['download.sh']
            cmd+=[dldir]
            cmd+=[dsid]
            proc=subprocess.run(cmd)
            if proc.returncode!=0: # cleanup
                print('Error Downloading!')
                return None

        return localpath

class WorkGraph:
    def __init__(self,nodes):
        self.nodes=nodes

    def run(self, input=None):
        for node in self.nodes:
            input=node.run(input)
            if input is None:
                raise RuntimeError('Stage returned error')