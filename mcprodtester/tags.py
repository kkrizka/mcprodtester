import yaml
import shlex

import pyAMI_atlas.api as AtlasAPI

def tag_valid(tag):
    """
    Check if a tag string is valid or not as "letter followed by an integer".
    """
    if tag[0].isalpha() and tag[1:].isdigit():
        return True
    return False

class Tag:
    """
    Describes the complete setup used to run a transform (aka a tag).

    Consists of the following properties:
    - productionStep: role of the tag in the production (ie. "recon")
    - release: the release Project,version (ie. `Athena,22.0.40`)
    - transform: the transform to run (ie: `Reco_tf`)
    - description: description of the tag (ie: "blah blah")
    - args: a dictionary of arguments to pass to the transform.

    The arguments are build from `args` as `--key value`. If `value` is a list,
    then the elements are added as `--key 'value1' 'value2' ...`.
    """
    def __init__(self,release,transform,productionStep=None,description='',args={}):
        self.productionStep=productionStep
        self.release=release
        self.transform=transform
        self.description=description
        self.args=args

    def buildargs(self,ATHENA_CORE_NUMBER=1):
        """
        Return the list of arguments that can then be passed to `subprocess`.

        Special processing:
         - If `$ATHENA_CORE_NUMBER` is found in the `athenaopts` argument, then
           it is replaced with the specified value.
        """
        escaper=str.maketrans({'\\':r'\\','"':r'\"'})
        args=[]
        for k,v in self.args.items():
            if v is None: # Skip blank
                continue

            listv=type(v) is list

            # Special replacements
            if k=='athenaopts':
                if listv:
                    v=map(lambda v: v.replace('$ATHENA_CORE_NUMBER',str(ATHENA_CORE_NUMBER)), v)
                else:
                    v=v.replace('$ATHENA_CORE_NUMBER',str(ATHENA_CORE_NUMBER))

            # Add to list
            args.append(f'--{k}')
            if listv:
                args+=[r'"{}"'.format(v0.translate(escaper)) for v0 in v]
            else:
                args.append(v)

        return args

class TagBuilder:
    """
    Build tags for requests by querying AMI and merging results with custom
    definitions.

    A tag can be obtained from the `tag` function.
    """
    def __init__(self, tags, ami):
        """
        Parameters
        ----------
        tags : dict
            dictionary containing custom tag definitions
        ami : pyAMI.client.Client
            AMI client to use for querying AMI.
        """
        # Store AMI client
        self.ami=ami

        self.tags={}
        for tname,tdata in tags.items():
            # Tag information that will be build in two steps:
            # 1. Get the tag from AMI (if `inherit` is present)
            # 2. Overwrite with any local definitions
            tag=None

            # Load data from AMI
            if 'inherit' in tdata:
                tag=self.amitag(tdata['inherit'])
            else:
                tag=Tag(None, None, {})

            # Override with local definitions
            tag.release       =tdata.get('release'       ,tag.release       )
            tag.transform     =tdata.get('transform'     ,tag.transform     )
            tag.productionStep=tdata.get('productionStep',tag.productionStep)
            tag.description   =tdata.get('description'   ,tag.description   )

            # Strip empty args and update non-empty
            for k,v in tdata.get('args',{}).items():
                if v==None:
                    tag.args.pop(k,None)
                else:
                    tag.args[k]=v

            # Save tag to local database
            self.tags[tname]=tag

    def amitag(self, tag):
        """
        Returns a `Tag` object by querying AMI. It does not merge with any
        custom definitions
        """
        prod=AtlasAPI.get_ami_tag(self.ami, tag)[0]

        trf=prod.pop('transformation')
        release=prod.pop('SWReleaseCache').replace('_',',')
        productionStep=prod.pop('productionStep')
        description=prod.pop('description')
        args={}

        # Pop keywords that are not arguments and save the rest
        special=['tagType', 'tagNumber', 'groupName', 'cacheName', 'baseRelease', 'transformationName', 'created', 'createdBy', 'modified', 'modifiedBy', 'transformation', 'SWReleaseCache']
        for kw in special:
            prod.pop(kw, None)

        # Remove empties
        prod={k:v for k,v in prod.items() if v!='NULL'}

        # Add remaining keys as args, making them ready for command line
        args.update(prod)

        # Build the object
        tobj=Tag(release, trf, args=args, productionStep=productionStep, description=description)
        return tobj

    def tag(self, tag):
        """
        Return the tag information for the given `tag` name.
        """
        # Locally defined
        if tag in self.tags: 
            return self.tags[tag]

        # Lookup in AMI
        tobj=self.amitag(tag)
        self.tags[tag]=tobj # cache

        return self.tags[tag]
