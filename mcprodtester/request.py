"""
Functions to help parsing the YAML request file.
"""

import yaml

def load(path):
    """
    Open a YAML request file and return the result.

    If any `include` directives are found, then they are merged recursively. The
    merging happens on the top-level blocks only. The following rules are
    applied:
        - if the block is a list, then the contents are appended to the end
        - if the block is a dict, then the contents are updated to the end 
    """
    request = {}
    with open(path, 'r') as fh:
        request = yaml.safe_load(fh)
        if 'include' in request:
            include = request.pop('include')
            for incpath in include:
                increquest = load(incpath)

                for k,v in increquest.items():
                    if k not in request:
                        # new key, just append!
                        request[k] = v
                        continue
                    if type(request[k])!=type(v):
                        raise ValueError(f"Incompatible types for key {k}")
                    if type(v)==list:
                        # append to list
                        request[k]+=v
                    elif type(v)==dict:
                        request[k].update(v)
    return request

