# MCProdTester

Tools to test a Monte Carlo production setup for during official requests. Covers all stages of MC production from generation to AOD production.

- Tracable definition of transform configurations and requests.
- Automated testing of full transform flow.
- Direct upload of new tags to AMI.

## Requirements
- CentOS7 (ie: `atlasadc/atlas-grid-centos7:latest` container)
- `/cvmfs` for Athena releases and necessary data files for production
- Python 3.6+

All Python dependencies are listed inside `requirements.txt` and are meant to be installed inside a virtual environment.

## Getting started
Setup the virtual environment will all dependencies and install a rucio configuration for accessing ATLAS data.

```shell
lsetup 'lcgenv -p LCG_101_ATLAS_14 x86_64-centos7-gcc11-opt Python'
python -m venv venv
source venv/bin/activate
pip install .
install -D rucio.cfg ${VIRTUAL_ENV}/etc/rucio.cfg
```

In new sessions, one only needs to activate the venv a
```shell
source venv/bin/activate
```

## Grid Certificate
A valid grid certificate proxy is required to access rucio (for input files) and AMI (for tag definitions).

The following alias will obtain a grid proxy using apptainer.
```shell
alias gridproxy='apptainer run --cleanenv -B /cvmfs:/cvmfs /cvmfs/unpacked.cern.ch/registry.hub.docker.com/atlasadc/atlas-grid-centos7:latest /bin/bash -c "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/emi/current-SL7/setup.sh && voms-proxy-init -voms atlas -valid 500:00"'
```

## Defining a Request
A request is defined inside a request YAML file. An example can be found in the `requests/run3minbias/request.yaml` file.

To define a new request, the YAML file should contain the following blocks:
- `workflows`: Defines the transform steps to process a sample.
- `samples`: Defines the list of samples to generate. Each sample is associated with a workflow.
- `tags` (optional): Defines custom tags for workflow steps.
- `include` (optional): Includes other YAML files.

### `workflows` block
The `workflows` block is key-value defintion of steps to process a sample. The key is the name of the workflow used for identification in the `samples` block. The value is the list of transform steps to execute sequentially

Each step is a dictionary with the following keys:
- `output`: The output type of the step (ie: any `X` that occurs as an `--outputXFile` option to to the transform command).
- `tag`: Tag that will be used to configure the transform command. It can either be an official tag from AMI or a custom tag definde in the `tags` block.

The steps are executed in order, with the output of step `n` being the input to step `n+1`. The output of the first step should always be an `EVNT`. This step has no input and uses `Gen_tf` to generate the first input file.

An example of a workflow named "custom" that runs `EVNT`->`HITS`->`AOD`.
```yaml
workflows:
  custom: # One tag is custom
    - output: EVNT
      tag: e8438
    - output: HITS
      tag: s3810
    - output: AOD
      tag: rMinBiasLowPt
```

### `samples` block
The `samples` block defines the list of samples to process. Each sample is a dictionary with the following keys:
- `dsid`: DSID of the sample. It can be either an existing or new DSID. For the new DSID's, the job options directory needs to be present in the same directory as the request file.
- `workflow`: The workflow to run for this sample.
- `scope` (optional): The rucio scope of the sample used to search for existing inputs. Default value is `mc16_13p6TeV`.

The same DSID can be processed by different workflows by using multiple entries in the `samples` list.

### `tags` block
The `tags` block can optionally be used to define custom tags outside of the official tags registered with AMI. The custom tags can be automatically registered in AMI after a request is tested.

The `tags` block is a dictionary with the key being the custom tag name and the value being the tag definition. The custom tag name can be referenced in the `workflows` block.

Each tag is a dictionary with the following keys:
- `description`: Description of the tag used when registering in AMI.
- `inherit`: An existing tag whose contents are copied to the custom tag. They can be overried by the other properties.
- `release`: The Athena release (ie: `Athena,22.0.83`)
- `transform`: The transform command used (ie: `Reco_tf`)
- `producitonStep`: Role of the tag in the production (ie: `recon`). Used when registering in AMI.
- `args`: A dictionary of arguments to pass to the transform. The arguments are build `--key value`. If `value` is a list, then the elements are added as `--key "value1" "value2" ...`. The surrounding quotes (`".."`) are added automatically.

An example of a custom tag that enabled low pt tracking on top of the existing `r13531` tag:
```yaml
tags:
  rMinBiasLowPt:
    inherit: r13531
    description: Clone of r13531 with low pt tracking
    args:
      preExec:
        - HITtoRDO:userRunLumiOverride={'run':330000, 'startmu':25.0,'endmu':52.0,'stepmu':1,'startlb':1,'timestamp':1560000000};from Digitization.DigitizationFlags import digitizationFlags;digitizationFlags.doPixelPlanarRadiationDamage=True
        - all:from AthenaConfiguration.AllConfigFlags import ConfigFlags; ConfigFlags.Trigger.AODEDMSet='AODFULL'
        - all:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doSlimming.set_Value_and_Lock(False); InDetFlags.doLowPt.set_Value_and_Lock(True);
```

### `include` block
If an `include` block is found, then they are merged recursively. The merging happens on the top-level blocks in each file only. The following rules are applied:
 - If the block is a list, then the contents are appended to the end.
 - If the block is a dictionary, then the contents are updated.

## Testing a Request
A request can be tested using the `test_request.py` program. Use the `--help` argument to see a list of options.

The basic usage is:
```bash
test_request.py requests/run3minbias/request.yaml
```

This runs through all samples sequentially and executes the necessary transforms defined in a workflow. Rucio is queried to determine any steps that already exist. The last skipped step is the one for which the dataset exists: `{scope}.{dsid}.*.{output}.{tag0}_{tag1}...`. The `{tag0}_{tag1}...` is an accumulation of tags from all previous steps. A random file is downloaded to serve as the next (first tested) step.

The following environmental variables can be used to configure the paths used by the `test_request.py` program.
- `WORK_DIR`: Location for working directories of transforms. Each transform is executed inside a folder corresponding the emulated rucio dataset name. Default value is `.cache/work`.
- `DATA_DIR`: Location for storing data. This includes both existing files downloaded from rucio and successful output from locally executed transforms. The local output is stored inside the `local` directory. Default value is `.cache`.

## Request Repository
Requests should be tracked using a git repository, referred to as the request repository. This tracks the history of the request definition and can automatically test each change using Continious Integration. A [template request repository](https://gitlab.cern.ch/kkrizka/mcrequest) is available.

CI can be added to a request repository using the following instruction.
1. Add the `MCProdTester` project as a git submodule to the request repository.
```bash
git submodule add https://gitlab.cern.ch/kkrizka/mcprodtester.git
```

2. Add a `.gitlab-ci.yml` with the following contents. 
```yaml
include:
  - project: 'kkrizka/MCProdTester'
    file: '/ci.yml'
    ref: 'main'

variables:
  MCPRODTESTER: './mcprodtester'
  GIT_SUBMODULE_STRATEGY: recursive
  REQUEST: request.yaml
```

3. Define the following variables inside Settings -> CI/CD of the request repository's GitLab web interface.
   - `GRID_CERT`: Base64 encoded value of your grid certificate (`base64 -i ~/.globus/usercern.pem`).
   - `GRID_KEY`: Base64 encoded value of your grid key (`base64 -i ~/.globus/userkey.pem`).
   - `GRID_PASS`: Your certificate password surrounded by `=`. (ie: `=password=`).

The following variables can be used to configure the CI template provided by the MCProdTester project.
- `MCPRODTESTER`: Location of the package relative to the root of the repository.
- `REQUEST`: Location of the request YAML file.

## Uploading Tags
Tags can be registered with AMI using the `update_tags.py`. The script takes the request YAML file as argument and processes all custom tags. If a tag matches the pattern `[a-z][0-9]+`, then the contents of an existing tag are updated instead.

On success, the output should look like:
```
Processing sIBL10
        The AMItag s3925 was inserted.
```

**Warning:** Make sure to write down the new tag names right away! You won't be able to find them otherwise.

