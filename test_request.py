#!/usr/bin/env python

# %% Important packages
import argparse

from pathlib import Path

import rucio.client
import pyAMI.client
import pyAMI_atlas.api as AtlasAPI

from mcprodtester import workflow
from mcprodtester import tags
from mcprodtester import request

# %% Input arguments
parser = argparse.ArgumentParser(description='Test sample request')
parser.add_argument('request' , help='Request file')
parser.add_argument('--sample', '-s', type=int, action='append', help='Sample index to run.')
parser.add_argument('--maxEvents', help='Maximum number of events.', default=5)
parser.add_argument('--cores','-j', help='Number of cores to utilize.', default=1)

args = parser.parse_args()

# %% Get request directory (ie: location of supporting files)
requestdir = Path(args.request).parent

# %% Open the request
request = request.load(args.request)

# %% Detremine samples to process
samples=request.get('samples',[])
if args.sample is not None:
    samples=map(samples.__getitem__, args.sample)

# %% Setup clients to access ATLAS databases
rucio = rucio.client.Client()
ami = pyAMI.client.Client('atlas')
AtlasAPI.init()

# %% Create the tag builder
builder = tags.TagBuilder(request.get('tags',{}), ami)

# %% Determine available inputs
workflows=[]

for sample in samples:
    dsid=sample['dsid']
    scope=sample.get('scope','mc16_13p6TeV')

    sampletags=[]
    mywf=[]

    genjo=None
    infile=None
    for stage in request['workflows'][sample['workflow']]:
        output=stage['output']
        stageargs=stage.get('args',{})

        if 'tag' in stage:
            sampletags.append(stage['tag'])

        if len(sampletags)>0:
            dsname=f'{scope}.{dsid}.*.{output}.{"_".join(sampletags)}'
        else:
            dsname=f'{scope}.{dsid}.*.{output}'
        dids=rucio.list_dids(scope, {'name':dsname})
        try:
            dsname=next(dids)
            infile=next(rucio.list_files(scope, dsname))
        except (StopIteration, IndexError):
            print(f'No files found for {dsname}')
            name=f'{dsid}.{"_".join(sampletags)}'
            mywf.append(workflow.TransformNode(name,sampletags[-1],output,tags=builder,args=stageargs,maxEvents=args.maxEvents,cores=args.cores))
            if output=='EVNT':
                # This is a Gen_tf that requires a JO input
                # Check if a new JO is specified (same dir as request yaml),
                # otherwise assume the DSID is already registered
                jopath=requestdir.joinpath(f'{dsid}')
                if jopath.exists():
                    genjo=jopath.resolve()
                else:
                    genjo=str(dsid)

    if infile is not None: # infile contains the last available file
        mywf.insert(0,workflow.RucioNode(scope=infile['scope'],dsid=infile['name'],client=rucio))

    # Create the workflow object
    if len(mywf)>0:
        wfgraph=workflow.WorkGraph(mywf)
        if genjo is not None:
            workflows.append((wfgraph,genjo))
        else:
            workflows.append(wfgraph)

# %% Execute all of the work graphs
for wf in workflows:
    if type(wf)==tuple: # we specified a starting input
        wf[0].run(wf[1])
    else:
        wf.run()
