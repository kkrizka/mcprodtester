#!/bin/bash

function usage() {
    echo "usage: ${0} Project,release command"
    exit 1
}

cores=1

while getopts "h:j:" arg; do
    case $arg in
	h)
	    usage
	    ;;
	j)
	    cores=${OPTARG}
	    ;;
    esac
done
shift $((OPTIND-1))

release=${1}

#
# Setup the environment
echo "Setup release ${release}"

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=${HOME}/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
source $AtlasSetup/scripts/asetup.sh ${release}

export ATHENA_CORE_NUMBER=${cores}

#
# Run the transform
eval "${@:2}"
